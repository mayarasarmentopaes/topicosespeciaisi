// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBywX88TdA94FVsD7QxuaMezh96IjltCe8',
    authDomain: 'topicosespeciaisi.firebaseapp.com',
    projectId: 'topicosespeciaisi',
    storageBucket: 'topicosespeciaisi.appspot.com',
    messagingSenderId: '726715690976',
    appId: '1:726715690976:web:9f4b8c70027cd634a54585',
    measurementId: 'G-G5NFPS8NE8'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
